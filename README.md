# trueyou-app
Find most similar Tinder profile with a set of celebrity images, return a face similarity score using AWS Rekognition.
 
# Prerequisites:
1. Create a virtualenv: `export VIRTUALENVWRAPPER_PYTHON=/usr/local/Cellar/python/2.7.13/bin/python2.7` containing the location of the Virtualenvwrapper exec.
2. Load virtualenvwrapper: `source /usr/local/bin/virtualenvwrapper.sh`
3. `mkvirtualenv NOME_VIRTUAL_ENV` and then `workon NOME_VIRTUAL_ENV`
4. Run `pip install -r requirements.txt`

# How to configure and use:
1. add Facebook credentials, connected with a Tinder account in `Tinder/config_copy.py` and rename it to `Tinder/config.py`
2. add AWS Rekognition credentials in `config_copy.py` and rename it to `config.py`
3. run `python find_matches.py`.

Output is a (JSON) comparision between the retrieved Tinder profiles photos and a set of celebrity photos.