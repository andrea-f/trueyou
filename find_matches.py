import os, sys
import datetime
from Tinder import tinder_api, config
import pprint
import json
import boto3
import datetime
import base64
import urllib
from config import secret_access_key_id, secret_access_key, region_name

class FindMatches(object):
	"""
	Finds matches in local Tinder after inputting fb pwd and username.
	"""

	def __init__(self):
		"""
		Initializes empty variables.
		"""
		self.client = None
		self.processed_pairs = []

	def get_rekognition_client(self):
		client = boto3.client('rekognition', 
			region_name=region_name, 
			aws_access_key_id=secret_access_key_id,
			aws_secret_access_key=secret_access_key
		)
		return client

	def login_tinder(self):
		"""
		Logs in to Tinder.
		"""
		print "Logging into Tinder..."
		return tinder_api.authverif()

	def load_matches(self, count=0, max_retry=2):
		"""
		Gets local matches.
		"""
		matches = tinder_api.get_recs_v2()
		#pprint.pprint(matches)
		if matches['meta']['status'] == 401:
			resp = self.login_tinder()
			if count < max_retry:
				self.load_matches(count+1)
			else:
				return []
		else:
			print "Found", len(matches), "matches."
			return matches

	def extract_relevant_info(self, matches):
		"""
		Extracts important information from the json result.
		"""
		users = []
		for match in matches['data']['results']:
			user = match['user']
			u = {
				"name": user['name'],
				"bio": user['bio'],
				"birthday": user['birth_date'],
				"gender": user['gender'], # 1 girl , 0 boy
				"jobs": user['jobs'],
				"teasers": match['teasers']
			}
			photos = []
			for p in user['photos']:
				resolutions = p['processedFiles']
				for res in resolutions:
					if res['height'] == 640:
						photos.append(res['url'])
			u['photos'] = photos
			users.append(u)
		print "Extracted relevant info from Tinder profiles", len(users)
		return users


	def compare_faces(
		self, 
		source_image, 
		target_image, 
		max_retry=3, 
		count=0, 
		source_name=None, 
		source_target=None
	):
		"""
		Find most similar profiles based on base 64 image.
		"""
		try:
			response = self.client.compare_faces(
				SourceImage={
					'Bytes': source_image
				},
				TargetImage={
					'Bytes': target_image
				},
				SimilarityThreshold=0.1
			)
		except Exception as e:
			print e, type(source_image), type(target_image), count, source_name, source_target
			print "Image may not contain face."
			if count < max_retry:
				return self.compare_faces(
					source_image, 
					target_image, 
					count=count+1, 
					source_name=source_name, 
					source_target=source_target
				)
			else:
				response = None
		extracted_similarity = self.extract_similarity(response)
		if len(extracted_similarity) >= 1:
			extracted_similarity = extracted_similarity[0]
		elif len(extracted_similarity) == 0:
			return None
		return extracted_similarity

	def extract_similarity(self, response):
		"""
		Extracts similarity key from response data.
		"""
		if isinstance(response, dict):
			similarities = []
			for item in response['FaceMatches']:
				similarities.append(item['Similarity'])
			return similarities
		return []

	def get_comparison_images(self, folder="./images/woman"):
		"""
		Load images from local folder.
		"""
		files_in_folder = {}
		for f in os.listdir(folder):
			if f.endswith((".jpg", ".png")) and not f in files_in_folder:
				full_path = folder+"/"+f
				files_in_folder[full_path] = self.open_file(full_path)
		print "Read %s comparison images from %s" % (len(files_in_folder), folder)
		return files_in_folder

	def convert_to_base64(self, image_file):
		"""
		Converts an image to base64 representation.
		"""
		with open(full_path, 'rb') as fn:
			base64_image = base64.b64encode(fn.read())
			return base64_image

	def open_file(self, file_name):
		"""
		Opens an image file to read in bytes.
		"""
		#print "Opening file:", file_name
		if "http" in file_name:
			image = urllib.urlopen(file_name).read()
			return image
		else:
			with open(file_name, 'rb') as fn:
				return fn.read()


	def get_most_similar_profiles(self, users, files_in_folder):
		"""
		Returns the most similar profiles from the possible Tinder matches.
		"""
		
		
		count_user = 0
		for user in users:
			count_photos = 0
			print "Processing", user['name'], str(count_user)+"/"+str(len(users))#str( float((count_user/len(users)) * 100))
			for photo in user['photos']:
				for k, v in files_in_folder.iteritems():
					# str(float(count_photos/(len(user['photos']) * len(files_in_folder) * 100 ))
					print "Comparing", k, photo, str(count_photos)+"/"+str((len(user['photos']) * len(files_in_folder))), user['name']
					photo_bytes = fm.open_file(photo)
					str_hash = k+photo
					if not str_hash in self.processed_pairs:
						similarity = fm.compare_faces(v, photo_bytes, source_name=k, source_target=photo)
						self.processed_pairs.append(str_hash)
						if similarity:
							print photo, k, similarity
							try:
								user['comparison'].append([photo, k, similarity])
							except:
								user['comparison'] = [[photo, k, similarity]]

					count_photos += 1
			count_user += 1
			with open("similarity_scraped_tinder.json", 'w') as f:
				f.write(json.dumps(users, indent=4))

		return users


	def create_html_with_results(self, file_name="similarity_scraped_tinder.json", html_output_file_name="html_output.html"):
		"""
		Creates a simple HTML page to test results.
		"""
		json_data = []
		html = """
		<html>
			<head>
				<title>
				Tinder profile celebrity similarity comparator
				</title>
			</head>
			<body>
		"""
		with open(file_name, 'r') as f:
			json_data = json.loads(f.read())
		if len(json_data) > 0:
			tables = []
			for item in json_data:
				item_text = """
				<hr>
				<div><b>Name:</b> %s</div>
				<div><b>Bio:</b> %s</div>
				<div><b>Birthday:</b> %s</div>
				""" % (item['name'], item['bio'], item['birthday'])
				if item.get('comparison', None):
					table_row = """
					<table style="width:100%">
					<tr>
						<th>Tinder image</th>
						<th>Reference image</th>
						<th>Similarity</th>
					</tr>
					"""
					rows = []
					for sub_item in item['comparison']:
						profile_image = sub_item[0]
						reference_image = sub_item[1]
						similarity = sub_item[2]
						prof_img = "<img src="+profile_image+" id='img' width='50%;' height='50%;' />" 
						ref_img = "<img src="+reference_image+" id='img'  width='50%;' height='50%;' />" 
						row = """
						<tr>
							<td>%s</td>
							<td>%s</td>
							<td><b>%s p/c</b></td>
						</tr>

						""" % (prof_img, ref_img, similarity)
						rows.append(row)
						
					rows.append("</table>")
				tab = item_text + table_row + '\n'.join(rows)
				tables.append(tab)
			tables_data = '\n'.join(tables)
		html_end = """
			</body>
			</html>
		"""
		page = html + tables_data + html_end
		page = page.encode('utf-8')
		with open(html_output_file_name, 'w') as f:
			f.write(page)



	def run_filter(self):
		"""
		1. Logins into Tinder with facebook credentials.
		2. Loads matches for logged in profile.
		3. Extracts relevant information from returned Tinder profiles.
		4. Loads comparison images.
		5. Checks every Tinder profile pic of a user with the reference set of comparison images.
		6. Saves the result into JSON.
		"""
		start = datetime.datetime.now()
		if self.client = None:
			self.client = self.get_rekognition_client()
		fm.login_tinder()
		matches = fm.load_matches()
		#pprint.pprint(matches)
		users = fm.extract_relevant_info(matches)
		pprint.pprint(users)
		files_in_folder = fm.get_comparison_images()
		new_users = fm.get_most_similar_profiles(users, files_in_folder)
		finish = datetime.datetime.now()
		elapsed_time = (finish - start).total_seconds() / 60 * 100
		print "Took", elapsed_time, "seconds to compare", len(users), "Tinder possible matches."

		now = str(datetime.datetime.now()).replace(" ","_")
		with open("similarity_scraped_tinder_%s.json" % now, 'w') as f:
			f.write(json.dumps(new_users, indent=4))


					



if __name__ == '__main__':
	fm = FindMatches()
	# Runs filter for similarity match:
	fm.run_filter()
	# Create html:
	fm.create_html_with_results()
	

	




